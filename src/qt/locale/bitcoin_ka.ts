<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ka">
<context>
    <name>AddressBookPage</name>
    <message>
        <location filename="../forms/addressbookpage.ui" line="+30"/>
        <source>Right-click to edit address or label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Create a new address</source>
        <translation>ახალი მისამართის შექმნა</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation>შექმ&amp;ნა</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy the currently selected address to the system clipboard</source>
        <translation>მონიშნული მისამართის კოპირება სისტემურ კლიპბორდში</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy</source>
        <translation>&amp;კოპირება</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>C&amp;lose</source>
        <translation>&amp;დახურვა</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="+90"/>
        <source>&amp;Copy Address</source>
        <translation>&amp;მისამართის კოპირება</translation>
    </message>
    <message>
        <location filename="../forms/addressbookpage.ui" line="-53"/>
        <source>Delete the currently selected address from the list</source>
        <translation>მონიშნული მისამართის წაშლა სიიდან</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Export the data in the current tab to a file</source>
        <translation>ამ ბარათიდან მონაცემების ექსპორტი ფაილში</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Export</source>
        <translation>&amp;ექსპორტი</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>&amp;Delete</source>
        <translation>&amp;წაშლა</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="-39"/>
        <source>Choose the address to send coins to</source>
        <translation>აირჩიეთ მონეტების გაგზავნის მისამართი</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose the address to receive coins with</source>
        <translation>აირჩიეთ მონეტების მიღების მისამართი</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>C&amp;hoose</source>
        <translation>&amp;არჩევა</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sending addresses</source>
        <translation>გაგზავნის მისამართი</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Receiving addresses</source>
        <translation>მიღების მისამართი</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>These are your Nexa addresses for sending payments. Always check the amount and the receiving address before sending coins.</source>
        <translation>ეს არის თქვენი Nexa-მისამართები გადახდების შესასრულებლად. მონეტების გაგზავნამდე ყოველთვის შეამოწმეთ თანხა და მიმღების მისამართი.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>These are your Nexa addresses for receiving payments. It is recommended to use a new receiving address for each transaction.</source>
        <translation>ეს არის თქვენი Nexa-მისამართები გადახდების მისაღებად. რეკომენდებულია ყოველი ტრანსაქციისათვის ახალი მიღების მისამართის გამოყენება.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy &amp;Label</source>
        <translation>ნიშნუ&amp;ლის კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation>რ&amp;ედაქტირება</translation>
    </message>
    <message>
        <location line="+173"/>
        <source>Export Address List</source>
        <translation>მისამართების სიის ექსპორტი</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>CSV-ფაილი (*.csv)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Exporting Failed</source>
        <translation>ექსპორტი ვერ განხორციელდა</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the address list to %1. Please try again.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddressTableModel</name>
    <message>
        <location filename="../addresstablemodel.cpp" line="+159"/>
        <source>Label</source>
        <translation>ნიშნული</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>მისამართი</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>(no label)</source>
        <translation>(არ არის ნიშნული)</translation>
    </message>
</context>
<context>
    <name>AskPassphraseDialog</name>
    <message>
        <location filename="../forms/askpassphrasedialog.ui" line="+26"/>
        <source>Passphrase Dialog</source>
        <translation>ფრაზა-პაროლის დიალოგი</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter passphrase</source>
        <translation>შეიყვანეთ ფრაზა-პაროლი</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>New passphrase</source>
        <translation>ახალი ფრაზა-პაროლი</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Repeat new passphrase</source>
        <translation>გაიმეორეთ ახალი ფრაზა-პაროლი</translation>
    </message>
    <message>
        <location filename="../askpassphrasedialog.cpp" line="+47"/>
        <source>Encrypt wallet</source>
        <translation>საფულის დაშიფრვა</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to unlock the wallet.</source>
        <translation>ეს ოპერაცია მოითხოვს თქვენი საფულის ფრაზა-პაროლს საფულის განსაბლოკად.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unlock wallet</source>
        <translation>საფულის განბლოკვა</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to decrypt the wallet.</source>
        <translation>ეს ოპერაცია მოითხოვს თქვენი საფულის ფრაზა-პაროლს საფულის გასაშიფრად.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Decrypt wallet</source>
        <translation>საფულის გაშიფრვა</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change passphrase</source>
        <translation>ფრაზა-პაროლის შეცვლა</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter the old passphrase and new passphrase to the wallet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Confirm wallet encryption</source>
        <translation>დაადასტურეთ საფულის დაშიფრვა</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: If you encrypt your wallet and lose your passphrase, you will &lt;b&gt;LOSE ALL OF YOUR NEXA COINS&lt;/b&gt;!</source>
        <translation>ყურადღება: საფულის დაშიფრვის შემდეგ თუ თქვენ დაკარგავთ ფრაზა-პაროლს,  &lt;b&gt;ყველა ბიტქოინი დაგეკარგებათ&lt;/b&gt;!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Are you sure you wish to encrypt your wallet?</source>
        <translation>დარწმუნებული ხართ, რომ გინდათ საფულის დაშიფრვა?</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 will close now to finish the encryption process. Remember that encrypting your wallet cannot fully protect your coins from being stolen by malware infecting your computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+109"/>
        <location line="+29"/>
        <source>Warning: The Caps Lock key is on!</source>
        <translation>ყურადღება: ჩართულია Caps Lock რეჟიმი!</translation>
    </message>
    <message>
        <location line="-140"/>
        <location line="+61"/>
        <source>Wallet encrypted</source>
        <translation>საფულე დაშიფრულია</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Enter the new passphrase to the wallet.&lt;br/&gt;Please use a passphrase of &lt;b&gt;ten or more random characters&lt;/b&gt;, or &lt;b&gt;eight or more words&lt;/b&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+84"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. For security reasons you should no longer keep previous unencrypted backups as your funds will be at risk if someone gains access to them.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+8"/>
        <location line="+42"/>
        <location line="+6"/>
        <source>Wallet encryption failed</source>
        <translation>ვერ მოხერხდა საფულის დაშიფრვა</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Wallet encryption failed due to an internal error. Your wallet was not encrypted.</source>
        <translation>საფულის დაშიფრვა ვერ მოხერხდა სისტემაში შეცდომის გამო. თქვენი საფულე არ არის დაშფრული.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+48"/>
        <source>The supplied passphrases do not match.</source>
        <translation>ფრაზა-პაროლები არ ემთხვევა ერთმანეთს.</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Wallet unlock failed</source>
        <translation>საფულის განბლოკვა ვერ მოხერხდა</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+11"/>
        <location line="+19"/>
        <source>The passphrase entered for the wallet decryption was incorrect.</source>
        <translation>საფულის განშიფრვის ფრაზა-პაროლი არაწორია</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Wallet decryption failed</source>
        <translation>საფულის განშიფრვა ვერ მოხერხდა</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Wallet passphrase was successfully changed.</source>
        <translation>საფულის ფრაზა-პაროლი შეცვლილია.</translation>
    </message>
</context>
<context>
    <name>BanTableModel</name>
    <message>
        <location filename="../bantablemodel.cpp" line="+86"/>
        <source>IP/Netmask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Banned Until</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ban Reason</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BitcoinGUI</name>
    <message>
        <location filename="../nexagui.cpp" line="+351"/>
        <source>Sign &amp;message...</source>
        <translation>ხელ&amp;მოწერა</translation>
    </message>
    <message>
        <location line="+435"/>
        <source>Synchronizing with network...</source>
        <translation>ქსელთან სინქრონიზება...</translation>
    </message>
    <message>
        <location line="-556"/>
        <source>&amp;Overview</source>
        <translation>მიმ&amp;ოხილვა</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Node</source>
        <translation>კვანძი</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Show general overview of wallet</source>
        <translation>საფულის ზოგადი მიმოხილვა</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Request payments (generates QR codes and %1: URIs)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Transactions</source>
        <translation>&amp;ტრანსაქციები</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse transaction history</source>
        <translation>ტრანსაქციების ისტორიის დათვალიერება</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Tokens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse or Send Tokens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Token History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse Token History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+36"/>
        <source>E&amp;xit</source>
        <translation>&amp;გასვლა</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit application</source>
        <translation>გასვლა</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>About &amp;Qt</source>
        <translation>&amp;Qt-ს შესახებ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about Qt</source>
        <translation>ინფორმაცია Qt-ს შესახებ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Options...</source>
        <translation>&amp;ოპციები</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify configuration options for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Unlimited...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify Bitcoin Unlimited Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Encrypt Wallet...</source>
        <translation>საფულის &amp;დაშიფრვა</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Backup Wallet...</source>
        <translation>საფულის &amp;არქივირება</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Restore Wallet...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Restore wallet from another location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Change Passphrase...</source>
        <translation>ფრაზა-პაროლის შე&amp;ცვლა</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sending addresses...</source>
        <translation>გაგზავნის მი&amp;სამართი</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Receiving addresses...</source>
        <translation>მიღების მისამა&amp;რთი</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open &amp;URI...</source>
        <translation>&amp;URI-ის გახსნა...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open a %1: URI or payment request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show the %1 help message to get a list with possible Nexa command-line options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+194"/>
        <source>%1 client</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location line="+193"/>
        <source>%n active connection(s) to Nexa network</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Importing blocks from disk...</source>
        <translation>ბლოკების იმპორტი დისკიდან...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindexing blocks on disk...</source>
        <translation>დისკზე ბლოკების რეინდექსაცია...</translation>
    </message>
    <message>
        <location line="-554"/>
        <source>Send coins to a Nexa address</source>
        <translation>მონეტების გაგზავნა Nexa-მისამართზე</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Backup wallet to another location</source>
        <translation>საფულის არქივირება სხვა ადგილზე</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Change the passphrase used for wallet encryption</source>
        <translation>საფულის დაშიფრვის ფრაზა-პაროლის შეცვლა</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Debug window</source>
        <translation>და&amp;ხვეწის ფანჯარა</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open debugging and diagnostic console</source>
        <translation>დახვეწისა და გიაგნოსტიკის კონსოლის გაშვება</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>&amp;Verify message...</source>
        <translation>&amp;ვერიფიკაცია</translation>
    </message>
    <message>
        <location line="+522"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="-785"/>
        <source>Wallet</source>
        <translation>საფულე</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>&amp;Send</source>
        <translation>&amp;გაგზავნა</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Receive</source>
        <translation>&amp;მიღება</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>&amp;Show / Hide</source>
        <translation>&amp;ჩვენება/დაფარვა</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show or hide the main Window</source>
        <translation>მთავარი ფანჯრის ჩვენება/დაფარვა</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Encrypt the private keys that belong to your wallet</source>
        <translation>თქვენი საფულის პირადი გასაღებების დაშიფრვა</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Sign messages with your Nexa addresses to prove you own them</source>
        <translation>მესიჯებზე ხელმოწერა თქვენი Nexa-მისამართებით იმის დასტურად, რომ ის თქვენია</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Verify messages to ensure they were signed with specified Nexa addresses</source>
        <translation>შეამოწმეთ, რომ მესიჯები ხელმოწერილია მითითებული Nexa-მისამართით</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>&amp;File</source>
        <translation>&amp;ფაილი</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Settings</source>
        <translation>&amp;პარამეტრები</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Help</source>
        <translation>&amp;დახმარება</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Tabs toolbar</source>
        <translation>ბარათების პანელი</translation>
    </message>
    <message>
        <location line="-95"/>
        <source>Show the list of used sending addresses and labels</source>
        <translation>გამოყენებული გაგზავნის მისამართებისა და ნიშნულების სიის ჩვენება</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show the list of used receiving addresses and labels</source>
        <translation>გამოყენებული მიღების მისამართებისა და ნიშნულების სიის ჩვენება</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Command-line options</source>
        <translation>საკომანდო სტრიქონის ოპ&amp;ციები</translation>
    </message>
    <message>
        <location line="+424"/>
        <source>No block source available...</source>
        <translation>ბლოკების წყარო მიუწვდომელია...</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>%1 behind</source>
        <translation>%1 გავლილია</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Last received block was generated %1 ago.</source>
        <translation>ბოლო მიღებული ბლოკის გენერირებიდან გასულია %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions after this will not yet be visible.</source>
        <translation>შემდგომი ტრანსაქციები ნაჩვენები ჯერ არ იქნება.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Error</source>
        <translation>შეცდომა</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning</source>
        <translation>გაფრთხილება</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Information</source>
        <translation>ინფორმაცია</translation>
    </message>
    <message>
        <location line="-86"/>
        <source>Up to date</source>
        <translation>განახლებულია</translation>
    </message>
    <message numerus="yes">
        <location line="-7"/>
        <source>Processed %n block(s) of transaction history.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Catching up...</source>
        <translation>ჩართვა...</translation>
    </message>
    <message>
        <location line="+164"/>
        <source>Date: %1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Amount: %1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type: %1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Label: %1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address: %1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent transaction</source>
        <translation>გაგზავნილი ტრანსაქციები</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Incoming transaction</source>
        <translation>მიღებული ტრანსაქციები</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>HD key generation is &lt;b&gt;enabled&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>HD key generation is &lt;b&gt;disabled&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt;</source>
        <translation>საფულე &lt;b&gt;დაშიფრულია&lt;/b&gt; და ამჟამად &lt;b&gt;განბლოკილია&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;locked&lt;/b&gt;</source>
        <translation>საფულე &lt;b&gt;დაშიფრულია&lt;/b&gt; და ამჟამად &lt;b&gt;დაბლოკილია&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>CoinControlDialog</name>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+14"/>
        <source>Coin Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Quantity:</source>
        <translation>რაოდენობა:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Bytes:</source>
        <translation>ბაიტები:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Amount:</source>
        <translation>თანხა:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Priority:</source>
        <translation>პრიორიტეტი:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Fee:</source>
        <translation>საკომისიო:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>დამატებითი საკომისიო:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>ხურდა:</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>(un)select max inputs</source>
        <translation>ყველას მონიშვნა/(მოხსნა)</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Tree mode</source>
        <translation>განტოტვილი</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>List mode</source>
        <translation>სია</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Amount</source>
        <translation>თანხა</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation>თარიღი</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmations</source>
        <translation>დადასტურება</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed</source>
        <translation>დადასტურებულია</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Priority</source>
        <translation>პრიორიტეტი</translation>
    </message>
    <message>
        <location filename="../coincontroldialog.cpp" line="+45"/>
        <source>Copy address</source>
        <translation>მისამართის კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>ნიშნულის კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+29"/>
        <source>Copy amount</source>
        <translation>თანხის კოპირება</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Copy transaction ID</source>
        <translation>ტრანსაქციის ID-ს კოპირება</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lock unspent</source>
        <translation>დაუხარჯავის დაბლოკვა</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unlock unspent</source>
        <translation>დაუხარჯავის განბლოკვა</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Copy quantity</source>
        <translation>რაოდენობის კოპირება</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation>საკომისიოს კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>დამატებითი საკომისიოს კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>ბაიტების კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>პრიორიტეტის კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>ხურდის კოპირება</translation>
    </message>
    <message>
        <location line="+352"/>
        <source>highest</source>
        <translation>უმაღლესი</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>higher</source>
        <translation>უფრო მაღალი</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>high</source>
        <translation>მაღალი</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium-high</source>
        <translation>საშუალოზე მაღალი</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium</source>
        <translation>საშუალო</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low-medium</source>
        <translation>საშუალოზე დაბალი</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low</source>
        <translation>დაბალი</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lower</source>
        <translation>უფრო დაბალი</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lowest</source>
        <translation>უდაბლესი</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>(%1 locked)</source>
        <translation>(%1 დაბლოკილია)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>none</source>
        <translation>ცარიელი</translation>
    </message>
    <message>
        <location line="+151"/>
        <source>yes</source>
        <translation>კი</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>no</source>
        <translation>არა</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>This label turns red if the transaction size is greater than 1000 bytes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+8"/>
        <source>This means a fee of at least %1 per kB is required.</source>
        <translation>ეს ნიშნავს, რომ კილობაიტზე საკომისიო იქნება მინიმუმ %1</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Can vary +/- 1 byte per input.</source>
        <translation>შეიძლება იყოს +/- 1 ბაიტი ყოველ შესავალზე.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transactions with higher priority are more likely to get included into a block.</source>
        <translation>მეტი პრიორიტეტის ტრანსაქციებს მეტი შანსი აქვს მოხვდეს ბლოკში.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This label turns red if the priority is smaller than &quot;medium&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>This label turns red if any recipient receives an amount smaller than %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Can vary +/- %1 satoshi(s) per input.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+48"/>
        <location line="+64"/>
        <source>(no label)</source>
        <translation>(არ არის ნიშნული)</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>change from %1 (%2)</source>
        <translation>ხურდა %1-დან (%2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(change)</source>
        <translation>(ხურდა)</translation>
    </message>
</context>
<context>
    <name>EditAddressDialog</name>
    <message>
        <location filename="../forms/editaddressdialog.ui" line="+14"/>
        <source>Edit Address</source>
        <translation>მისამართის შეცვლა</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Label</source>
        <translation>ნიშნუ&amp;ლი</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The label associated with this address list entry</source>
        <translation>მისამართების სიის ამ ჩანაწერთან ასოცირებული ნიშნული</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>The address associated with this address list entry. This can only be modified for sending addresses.</source>
        <translation>მისამართების სიის ამ ჩანაწერთან მისამართი ასოცირებული. მისი შეცვლა შეიძლება მხოლოდ გაგზავნის მისამართის შემთხვევაში.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>&amp;Address</source>
        <translation>მის&amp;ამართი</translation>
    </message>
    <message>
        <location filename="../editaddressdialog.cpp" line="+25"/>
        <source>New receiving address</source>
        <translation>ახალი მიღების მისამართი</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New sending address</source>
        <translation>ახალი გაგზავნის მისამართი</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit receiving address</source>
        <translation>მიღების მისამართის შეცვლა</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit sending address</source>
        <translation>გაგზავნის მისამართის შეცვლა</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>The entered address &quot;%1&quot; is already in the address book.</source>
        <translation>მისამართი &quot;%1&quot; უკვე არის მისამართების წიგნში.</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>The entered address &quot;%1&quot; is not a valid Nexa address.</source>
        <translation>შეყვანილი მისამართი &quot;%1&quot; არ არის ვალიდური Nexa-მისამართი.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not unlock wallet.</source>
        <translation>საფულის განბლოკვა ვერ მოხერხდა.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New key generation failed.</source>
        <translation>ახალი გასაღების გენერირება ვერ მოხერხდა</translation>
    </message>
</context>
<context>
    <name>EditTokenDialog</name>
    <message>
        <location filename="../forms/edittokendialog.ui" line="+14"/>
        <source>Edit Token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Group ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The group ID of the token to be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../edittokendialog.cpp" line="+25"/>
        <source>Add new token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove existing token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Token Dialog Error! Please notify a developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+20"/>
        <source>Group ID required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Failed to add token: Invalid Group ID.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed to add token.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Failed to remove token.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FreespaceChecker</name>
    <message>
        <location filename="../intro.cpp" line="+75"/>
        <source>A new data directory will be created.</source>
        <translation>შეიქმნება ახალი მონაცემთა კატალოგი.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>name</source>
        <translation>სახელი</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Directory already exists. Add %1 if you intend to create a new directory here.</source>
        <translation>კატალოგი უკვე არსებობს. დაამატეთ %1 თუ გინდათ ახალი კატალოგის აქვე შექმნა.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Path already exists, and is not a directory.</source>
        <translation>მისამართი უკვე არსებობს და არ წარმოადგენს კატალოგს.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot create data directory here.</source>
        <translation>კატალოგის აქ შექმნა შეუძლებელია.</translation>
    </message>
</context>
<context>
    <name>HelpMessageDialog</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+39"/>
        <source>version</source>
        <translation>ვერსია</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+2"/>
        <source>(%1-bit)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Command-line options</source>
        <translation>კომანდების ზოლის ოპციები</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Usage:</source>
        <translation>გამოყენება:</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>command-line options</source>
        <translation>კომანდების ზოლის ოპციები</translation>
    </message>
</context>
<context>
    <name>Intro</name>
    <message>
        <location filename="../forms/intro.ui" line="+14"/>
        <source>Welcome</source>
        <translation>მოგესალმებით</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Welcome to %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>As this is the first time the program is launched, you can choose where %1 will store its data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 will download and store a copy of the Nexa block chain. At least %2GB of data will be stored in this directory, and it will grow over time. The wallet will also be stored in this directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Use the default data directory</source>
        <translation>ნაგულისხმევი კატალოგის გამოყენება</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use a custom data directory:</source>
        <translation>მითითებული კატალოგის გამოყენება:</translation>
    </message>
    <message>
        <location filename="../intro.cpp" line="+81"/>
        <source>Error: Specified data directory &quot;%1&quot; cannot be created.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error</source>
        <translation>შეცდომა</translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%n GB of free space available</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>(of %n GB needed)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessThanValidator</name>
    <message>
        <location filename="../unlimiteddialog.cpp" line="+486"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModalOverlay</name>
    <message>
        <location filename="../forms/modaloverlay.ui" line="+14"/>
        <source>Form</source>
        <translation type="unfinished">ფორმა</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet. This means that recent transactions will not be visible, and the balance will not be up-to-date until this process has completed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Spending coins may not be possible during that phase!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Amount of blocks left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+26"/>
        <source>unknown...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Last block time</source>
        <translation type="unfinished">ბოლო ბლოკის დრო</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>~</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Progress increase per Hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+20"/>
        <source>calculating...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Estimated time left until synced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../modaloverlay.cpp" line="+140"/>
        <source>Unknown. Reindexing (%1)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Reindexing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown. Syncing Headers (%1)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Syncing Headers...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenURIDialog</name>
    <message>
        <location filename="../forms/openuridialog.ui" line="+14"/>
        <source>Open URI</source>
        <translation>URI-ის გახსნა</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open payment request from URI or file</source>
        <translation>გადახდის მოთხოვნის შექმნა URI-იდან ან ფაილიდან</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>URI:</source>
        <translation>URI:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Select payment request file</source>
        <translation>გადახდის მოთხოვნის ფაილის არჩევა</translation>
    </message>
    <message>
        <location filename="../openuridialog.cpp" line="+40"/>
        <source>Select payment request file to open</source>
        <translation>გადახდის მოთხოვნის ფაილის არჩევა გასახსნელად</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../forms/optionsdialog.ui" line="+14"/>
        <source>Options</source>
        <translation>ოპციები</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Main</source>
        <translation>&amp;მთავარი</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Number of script &amp;verification threads</source>
        <translation>სკრიპტის &amp;ვერიფიცირების ნაკადების რაოდენობა</translation>
    </message>
    <message>
        <location line="+271"/>
        <location line="+187"/>
        <source>IP address of the proxy (e.g. IPv4: 127.0.0.1 / IPv6: ::1)</source>
        <translation>პროქსის IP-მისამართი (მაგ.: IPv4: 127.0.0.1 / IPv6: ::1)</translation>
    </message>
    <message>
        <location line="+164"/>
        <location line="+13"/>
        <source>Third party URLs (e.g. a block explorer) that appear in the transactions tab as context menu items. %s in the URL is replaced by transaction hash. Multiple URLs are separated by vertical bar |.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Third party transaction URLs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Active command-line options that override above options:</source>
        <translation>საკომანდო სტრიქონის აქტიური ოპციები, რომლებიც გადაფარავენ ზემოთნაჩვენებს:</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Reset all client options to default.</source>
        <translation>კლიენტის ყველა პარამეტრის დაბრუნება ნაგულისხმევ მნიშვნელობებზე.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>დაბ&amp;რუნების ოპციები</translation>
    </message>
    <message>
        <location line="-504"/>
        <source>&amp;Network</source>
        <translation>&amp;ქსელი</translation>
    </message>
    <message>
        <location line="-252"/>
        <source>Automatically start %1 after logging in to the system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Start %1 on system login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a, one time only, full database reindex on the next startup.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindex on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+56"/>
        <source>(0 = auto, &lt;0 = leave that many cores free)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+38"/>
        <source>W&amp;allet</source>
        <translation>ს&amp;აფულე</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Expert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Enable coin &amp;control features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>If you disable the spending of unconfirmed change, the change from a transaction cannot be used until that transaction has at least one confirmation. This also affects how your balance is computed.</source>
        <translation>დაუდასტურებელი ხურდის გამოყენების აკრძალვის შემდეგ მათი გამოყენება შეუძლებელი იქნება, სანამ ტრანსაქციას არ ექნება ერთი დასტური მაინც. ეს აისახება თქვენი ნაშთის დათვლაზეც.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Spend unconfirmed change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Instant Transactions is enabled you can spend unconfirmed transactions immediately.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Instant Transactions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When creating and sending transactions, auto consolidate will, if required, automatically create a chain of transactions which have inputs no greater than the consensus input limit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Auto Consolidate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+39"/>
        <source>&amp;Rescan wallet on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Token Whitelist is enabled you can control what tokens appear in your wallet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Token Whitelist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Automatically open the Nexa client port on the router. This only works when your router supports UPnP and it is enabled.</source>
        <translation>როუტერში Nexa-კლიენტის პორტის ავტომატური გახსნა. მუშაობს, თუ თქვენს როუტერს ჩართული აქვს UPnP.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Map port using &amp;UPnP</source>
        <translation>პორტის გადამისამართება &amp;UPnP-ით</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Accept connections from outside.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Allow incoming connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Connect to the Nexa network through a SOCKS5 proxy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Connect through SOCKS5 proxy (default proxy):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+187"/>
        <source>Proxy &amp;IP:</source>
        <translation>პროქსის &amp;IP:</translation>
    </message>
    <message>
        <location line="-155"/>
        <location line="+187"/>
        <source>&amp;Port:</source>
        <translation>&amp;პორტი</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>Port of the proxy (e.g. 9050)</source>
        <translation>პროქსის პორტი (მაგ.: 9050)</translation>
    </message>
    <message>
        <location line="-163"/>
        <source>Used for reaching peers via:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+23"/>
        <location line="+23"/>
        <source>Shows if the supplied default SOCKS5 proxy is used to reach peers via this network type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-36"/>
        <source>IPv4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Connect to the Nexa network through a separate SOCKS5 proxy for Tor hidden services.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+102"/>
        <source>&amp;Window</source>
        <translation>&amp;ფანჯარა</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show only a tray icon after minimizing the window.</source>
        <translation>ფანჯრის მინიმიზებისას მხოლოდ იკონა სისტემურ ზონაში</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize to the tray instead of the taskbar</source>
        <translation>&amp;მინიმიზება სისტემურ ზონაში პროგრამების პანელის ნაცვლად</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Minimize instead of exit the application when the window is closed. When this option is enabled, the application will be closed only after selecting Exit in the menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>M&amp;inimize on close</source>
        <translation>მ&amp;ინიმიზება დახურვისას</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Display</source>
        <translation>&amp;ჩვენება</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>User Interface &amp;language:</source>
        <translation>სამომხმარებ&amp;ლო ენა:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The user interface language can be set here. This setting will take effect after restarting %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Unit to show amounts in:</source>
        <translation>ერთეუ&amp;ლი:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Choose the default subdivision unit to show in the interface and when sending coins.</source>
        <translation>აირჩიეთ გასაგზავნი თანხის ნაგულისხმევი ერთეული.</translation>
    </message>
    <message>
        <location line="-518"/>
        <source>Whether to show coin control features or not.</source>
        <translation>ვაჩვენოთ თუ არა მონეტების მართვის პარამეტრები.</translation>
    </message>
    <message>
        <location line="-33"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a full blockchain resynchronization on the next startup (one time only).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Resynchronize block data on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+710"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>&amp;გაუქმება</translation>
    </message>
    <message>
        <location filename="../optionsdialog.cpp" line="+135"/>
        <source>default</source>
        <translation>ნაგულისხმევი</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>none</source>
        <translation>ცარიელი</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Confirm options reset</source>
        <translation>დაადასტურეთ პარამეტრების დაბრუნება ნაგულისხმევზე</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+27"/>
        <source>Client restart required to activate changes.</source>
        <translation>ცვლილებები ძალაში შევა კლიენტის ხელახალი გაშვების შემდეგ.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <source>This change would require a client restart.</source>
        <translation>ამ ცვლილებების ძალაში შესასვლელად საჭიროა კლიენტის დახურვა და ხელახალი გაშვება.</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>The supplied proxy address is invalid.</source>
        <translation>პროქსის მისამართი არასწორია.</translation>
    </message>
</context>
<context>
    <name>OverviewPage</name>
    <message>
        <location filename="../forms/overviewpage.ui" line="+14"/>
        <source>Form</source>
        <translation>ფორმა</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Balances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+386"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet.</source>
        <translation>ნაჩვენები ინფორმაცია შეიძლება მოძველებული იყოს. თქვენი საფულე ავტომატურად სინქრონიზდება Nexa-ის ქსელთან კავშირის დამყარების შემდეგ, ეს პროცესი ჯერ არ არის დასრულებული.</translation>
    </message>
    <message>
        <location line="-333"/>
        <source>Unconfirmed transactions to watch-only addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Mined balance in watch-only addresses that has not yet matured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Current total balance in watch-only addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Watch-only:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Available:</source>
        <translation>ხელმისაწვდომია:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current spendable balance</source>
        <translation>თქვენი ხელმისაწვდომი ნაშთი</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Your current balance in watch-only addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Pending:</source>
        <translation>იგზავნება:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Spendable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Recent transactions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-292"/>
        <source>Total of transactions that have yet to be confirmed, and do not yet count toward the spendable balance</source>
        <translation>დასადასტურებელი ტრანსაქციების საერთო რაოდენობა, რომლებიც ჯერ არ არის ასახული ბალანსში</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Immature:</source>
        <translation>მოუმზადებელია:</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Mined balance that has not yet matured</source>
        <translation>მოპოვებული თანხა, რომელიც ჯერ არ არის მზადყოფნაში</translation>
    </message>
    <message>
        <location line="-16"/>
        <source>Total:</source>
        <translation>სულ:</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Your current total balance</source>
        <translation>თქვენი სრული მიმდინარე ბალანსი</translation>
    </message>
</context>
<context>
    <name>PaymentServer</name>
    <message>
        <location filename="../paymentserver.cpp" line="+470"/>
        <location line="+13"/>
        <location line="+10"/>
        <source>URI handling</source>
        <translation>URI-ების დამუშავება</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Invalid payment address %1</source>
        <translation>გადახდის მისამართი არასწორია: %1</translation>
    </message>
    <message>
        <location line="+180"/>
        <source>Requested payment amount of %1 is too small (considered dust).</source>
        <translation>მოთხოვნილი გადახდის %1 მოცულობა ძალიან მცირეა (ითვლება &quot;მტვრად&quot;)</translation>
    </message>
    <message>
        <location line="-288"/>
        <location line="+240"/>
        <location line="+47"/>
        <location line="+126"/>
        <location line="+15"/>
        <location line="+15"/>
        <source>Payment request error</source>
        <translation>გადახდის მოთხოვნის შეცდომა</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Cannot start click-to-pay handler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Payment request fetch URL is invalid: %1</source>
        <translation>არასწორია გადახდის მოთხოვნის URL: %1</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>URI cannot be parsed! This can be caused by an invalid Nexa address or malformed URI parameters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Payment request file handling</source>
        <translation>გადახდის მოთხოვნის ფაილის დამუშავება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Payment request file cannot be read! This can be caused by an invalid payment request file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+65"/>
        <location line="+11"/>
        <location line="+35"/>
        <location line="+12"/>
        <location line="+21"/>
        <location line="+98"/>
        <source>Payment request rejected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Payment request network doesn&apos;t match client network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Payment request expired.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Payment request is not initialized.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Unverified payment requests to custom payment scripts are unsupported.</source>
        <translation>არავერიფიცირებული გადახდის მოთხოვნები გადახდის სამომხმარებლო სკრიპტებისათვის არ არის მხარდაჭერილი.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+21"/>
        <source>Invalid payment request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Refund from %1</source>
        <translation>დაბრუნება %1-საგან</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Payment request %1 is too large (%2 bytes, allowed %3 bytes).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error communicating with %1: %2</source>
        <translation>ვერ გამოდის კავშირზე %1: %2</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Payment request cannot be parsed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Bad response from server %1</source>
        <translation>ცუდი პასუხი სერვერისაგან %1</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Payment acknowledged</source>
        <translation>გადახდა მიღებულია</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Network request error</source>
        <translation>ქსელური მოთხოვნის შეცდომა</translation>
    </message>
</context>
<context>
    <name>PeerTableModel</name>
    <message>
        <location filename="../peertablemodel.cpp" line="+120"/>
        <source>Node/Service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ping Time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../nexaunits.cpp" line="+187"/>
        <source>Amount</source>
        <translation>თანხა</translation>
    </message>
    <message>
        <location filename="../guiutil.cpp" line="+135"/>
        <source>Enter a NEXA address (e.g. %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+831"/>
        <source>%1 d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 h</source>
        <translation>%1 სთ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 m</source>
        <translation>%1 წთ</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+68"/>
        <source>%1 s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-11"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>N/A</source>
        <translation>მიუწვდ.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location line="+19"/>
        <source>%n seconds(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minutes(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n day(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <location line="+8"/>
        <source>%n week(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="-2"/>
        <source>%1 and %2</source>
        <translation type="unfinished">%1 და %2</translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%n year(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QRImageWidget</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+36"/>
        <source>&amp;Save Image...</source>
        <translation>გამო&amp;სახულების შენახვა...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation>გამოსახულების &amp;კოპირება</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Save QR Code</source>
        <translation>QR-კოდის შენახვა</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation>PNG სურათი (*.png)</translation>
    </message>
</context>
<context>
    <name>RPCConsole</name>
    <message>
        <location filename="../forms/debugwindow.ui" line="+239"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+17"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+19"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+1753"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+23"/>
        <source>N/A</source>
        <translation>მიუწვდ.</translation>
    </message>
    <message>
        <location line="-2715"/>
        <source>Client version</source>
        <translation>კლიენტის ვერსია</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>&amp;Information</source>
        <translation>&amp;ინფორმაცია</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Debug window</source>
        <translation>დახვეწის ფანჯარა</translation>
    </message>
    <message>
        <location line="+620"/>
        <source>General</source>
        <translation>საერთო</translation>
    </message>
    <message>
        <location line="-570"/>
        <source>Startup time</source>
        <translation>სტარტის დრო</translation>
    </message>
    <message>
        <location line="+474"/>
        <source>Network</source>
        <translation>ქსელი</translation>
    </message>
    <message>
        <location line="-467"/>
        <source>Name</source>
        <translation>სახელი</translation>
    </message>
    <message>
        <location line="-34"/>
        <location line="+2390"/>
        <source>User Agent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2380"/>
        <source>Using BerkeleyDB version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Datadir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Number of connections</source>
        <translation>შეერთებების რაოდენობა</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Last block time (time since)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last block size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Tx pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Orphan pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Messages in CAPD pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - usage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - txns per second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (Totals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (24-Hour Averages)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (Totals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (24-Hour Averages)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (Totals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (24-Hour Averages)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Open the %1 debug log file from the current data directory. This can take a few seconds for large log files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Block Propagation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Transaction Pools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Block chain</source>
        <translation>ბლოკთა ჯაჭვი</translation>
    </message>
    <message>
        <location line="+476"/>
        <location line="+1694"/>
        <source>Received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1614"/>
        <location line="+1591"/>
        <source>Sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1550"/>
        <source>&amp;Transaction Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Instantaneous Rate (1s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+536"/>
        <source>Peak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-463"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Runtime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>24-Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Displayed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-769"/>
        <location line="+536"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-265"/>
        <source>Smoothed Rate (60s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+539"/>
        <source>&amp;Peers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Banned peers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+57"/>
        <location filename="../rpcconsole.cpp" line="+311"/>
        <location line="+856"/>
        <source>Select a peer to view detailed information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Whitelisted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Starting Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Headers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Ban Score</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Connection Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Receive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Ping Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>The duration of a currently outstanding ping.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ping Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Time Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2650"/>
        <source>Current number of blocks</source>
        <translation>ბლოკების მიმდინარე რაოდენობა</translation>
    </message>
    <message>
        <location line="+126"/>
        <source>&amp;Open</source>
        <translation>&amp;შექმნა</translation>
    </message>
    <message>
        <location line="+431"/>
        <source>&amp;Console</source>
        <translation>&amp;კონსოლი</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Decrease font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Increase font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+127"/>
        <source>&amp;Network Traffic</source>
        <translation>&amp;ქსელის ტრაფიკი</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>&amp;Clear</source>
        <translation>&amp;წაშლა</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Totals</source>
        <translation>სულ:</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-530"/>
        <source>In:</source>
        <translation>შემომავალი:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Out:</source>
        <translation>გამავალი:</translation>
    </message>
    <message>
        <location filename="../forms/debugwindow.ui" line="-704"/>
        <source>Debug log file</source>
        <translation>დახვეწის ლოგ-ფაილი</translation>
    </message>
    <message>
        <location line="+541"/>
        <source>Clear console</source>
        <translation>კონსოლის გასუფთავება</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-207"/>
        <source>&amp;Disconnect Node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Ban Node for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-3"/>
        <source>1 &amp;hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+48"/>
        <source>&amp;Unban Node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Welcome to the %1 RPC console.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Use up and down arrows to navigate history, and &lt;b&gt;Ctrl-L&lt;/b&gt; to clear screen.</source>
        <translation>კლავიშები &quot;ზევით&quot; და &quot;ქვევით&quot; - ისტორიაში მოძრაობა, &lt;b&gt;Ctrl-L&lt;/b&gt; - ეკრანის გასუფთავება.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</source>
        <translation>აკრიფეთ &lt;b&gt;help&lt;/b&gt; ფაშვებული ბრძანებების სანახავად.</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+110"/>
        <source>%1 B</source>
        <translation>%1 B</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>(node id: %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>via %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>never</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Inbound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Outbound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+6"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReceiveCoinsDialog</name>
    <message>
        <location filename="../forms/receivecoinsdialog.ui" line="+166"/>
        <source>&amp;Amount:</source>
        <translation>თ&amp;ანხა:</translation>
    </message>
    <message>
        <location line="-113"/>
        <source>&amp;Label:</source>
        <translation>ნიშნუ&amp;ლი:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>&amp;Message:</source>
        <translation>&amp;მესიჯი:</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Reuse one of the previously used receiving addresses. Reusing addresses has security and privacy issues. Do not use this unless re-generating a payment request made before.</source>
        <translation>რომელიმე ადრე გამოყენებული მიღების მისამართის გამოყენება. ეს ამცირებს უსაფრთხოებასა და პრივატულობას. ნუ გამოიყენებთ ამ ოპციას, თუ არ ახდენთ ადრე მოთხოვნილი გადახდის ხელახლა გენერირებას.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>R&amp;euse an existing receiving address (not recommended)</source>
        <translation>ად&amp;რე გამოყენებული მიღების მისამართის გამოყენება (არ არის რეკომენდებული)</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+110"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened. Note: The message will not be sent with the payment over the Nexa network.</source>
        <translation>არააუცილებელი მესიჯი, რომელიც ერთვის გადახდის მოთხოვნას და ნაჩვენები იქნება მოთხოვნის გახსნისას. შენიშვნა: მესიჯი არ გაყვება გადახდას ბითქოინის ქსელში.</translation>
    </message>
    <message>
        <location line="-136"/>
        <location line="+106"/>
        <source>An optional label to associate with the new receiving address.</source>
        <translation>არააუცილებელი ნიშნული ახალ მიღების მისამართთან ასოცირებისათვის.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Use this form to request payments. All fields are &lt;b&gt;optional&lt;/b&gt;.</source>
        <translation>გამოიყენეთ ეს ფორმა გადახდის მოთხოვნისათვის. ყველა ველი &lt;b&gt;არააუცილებელია&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="-136"/>
        <location line="+120"/>
        <source>An optional amount to request. Leave this empty or zero to not request a specific amount.</source>
        <translation>მოთხოვნის მოცულობა. არააუცილებელია. ჩაწერეთ 0 ან დატოვეთ ცარიელი, თუ არ მოითხოვება კონკრეტული მოცულობა.</translation>
    </message>
    <message>
        <location line="-46"/>
        <source>Clear all fields of the form.</source>
        <translation>ფორმის ყველა ველის წაშლა</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>წაშლა</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Requested payments history</source>
        <translation>მოთხოვნილი გადახდების ისტორია</translation>
    </message>
    <message>
        <location line="-132"/>
        <source>&amp;Request payment</source>
        <translation>&amp;გადახდის მოთხოვნა</translation>
    </message>
    <message>
        <location line="+157"/>
        <source>Show the selected request (does the same as double clicking an entry)</source>
        <translation>არჩეული მოთხოვნის ჩვენება (იგივეა, რაც ჩანაწერზე ორჯერ ჩხვლეტა)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show</source>
        <translation>ჩვენება</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Remove the selected entries from the list</source>
        <translation>მონიშნული ჩანაწერების წაშლა სიიდან</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove</source>
        <translation>წაშლა</translation>
    </message>
    <message>
        <location filename="../receivecoinsdialog.cpp" line="+50"/>
        <source>Copy URI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>ნიშნულის კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy message</source>
        <translation>მესიჯის კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>თანხის კოპირება</translation>
    </message>
</context>
<context>
    <name>ReceiveRequestDialog</name>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="+29"/>
        <source>QR Code</source>
        <translation>QR-კოდი</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Copy &amp;URI</source>
        <translation>&amp;URI-ის კოპირება</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Copy &amp;Address</source>
        <translation>მის&amp;ამართის კოპირება</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Save Image...</source>
        <translation>გამო&amp;სახულების შენახვა...</translation>
    </message>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+78"/>
        <source>Request payment to %1</source>
        <translation>%1-ის გადაზდის მოთხოვნა</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment information</source>
        <translation>ინფორმაცია გადახდის შესახებ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address</source>
        <translation>მისამართი</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount</source>
        <translation>თანხა</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Label</source>
        <translation>ნიშნული</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Message</source>
        <translation>მესიჯი</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Resulting URI too long, try to reduce the text for label / message.</source>
        <translation>URI ძალიან გრძელი გამოდის, შეამოკლეთ ნიშნულის/მესიჯის ტექსტი.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error encoding URI into QR Code.</source>
        <translation>შედომა URI-ის QR-კოდში გადაყვანისას.</translation>
    </message>
</context>
<context>
    <name>RecentRequestsTableModel</name>
    <message>
        <location filename="../recentrequeststablemodel.cpp" line="+30"/>
        <source>Date</source>
        <translation>თარიღი</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Label</source>
        <translation>ნიშნული</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message</source>
        <translation>მესიჯი</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Amount</source>
        <translation>თანხა</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>(no label)</source>
        <translation>(არ არის ნიშნული)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>(no message)</source>
        <translation>(მესიჯები არ არის)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>(no amount)</source>
        <translation>(თანხა არ არის)</translation>
    </message>
</context>
<context>
    <name>SendCoinsDialog</name>
    <message>
        <location filename="../forms/sendcoinsdialog.ui" line="+14"/>
        <location filename="../sendcoinsdialog.cpp" line="+626"/>
        <source>Send Coins</source>
        <translation>მონეტების გაგზავნა</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Coin Control Features</source>
        <translation>მონეტების კონტროლის პარამეტრები</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Select specific coins you want to use in this transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Inputs...</source>
        <translation>ხარჯები...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>automatically selected</source>
        <translation>არჩეულია ავტომატურად</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Insufficient funds!</source>
        <translation>არ არის საკმარისი თანხა!</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Quantity:</source>
        <translation>რაოდენობა:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Bytes:</source>
        <translation>ბაიტები:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Amount:</source>
        <translation>თანხა:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Priority:</source>
        <translation>პრიორიტეტი:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Fee:</source>
        <translation>საკომისიო:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>დამატებითი საკომისიო:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>ხურდა:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation>ამის გააქტიურებისას თუ ხურდის მისამართი ცარიელია ან არასწორია, ხურდა გაიგზავნება ახლად გენერირებულ მისამართებზე.</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+16"/>
        <source>Custom change address</source>
        <translation>ხურდის მისამართი</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>Transaction Fee:</source>
        <translation>ტრანსაქციის საფასური - საკომისიო:</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Choose your transaction fee.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+37"/>
        <source>collapse fee-settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+48"/>
        <location line="+16"/>
        <source>If the custom fee is set to 1000 satoshis and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 satoshis in fee, while &quot;total at least&quot; pays 1000 satoshis. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-13"/>
        <source>per kilobyte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>total at least</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Custom fee amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+13"/>
        <source>Paying only the minimum fee is just fine as long as there is less transaction volume than space in the blocks. But be aware that this can end up in a never confirming transaction once there is more demand for nexa transactions than the network can process.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>(read the tooltip)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Use the recommended fee amount.  You can select a faster or slower confirmation time by moving the &quot;Confirmation Time&quot; slider.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Recommended:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Choose a custom fee amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+52"/>
        <source>(Smart fee not initialized yet. This usually takes a few blocks...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <location line="+33"/>
        <source>Transaction confirmation time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Confirmation time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+63"/>
        <source>normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>fast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Send as zero-fee transaction if possible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(confirmation may take longer)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Send to multiple recipients at once</source>
        <translation>გაგზავნა რამდენიმე რეციპიენტთან ერთდროულად</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Recipient</source>
        <translation>&amp;რეციპიენტის დამატება</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Clear all fields of the form.</source>
        <translation>ფორმის ყველა ველის წაშლა</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear &amp;All</source>
        <translation>გ&amp;ასუფთავება</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Current Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Balance:</source>
        <translation>ბალანსი:</translation>
    </message>
    <message>
        <location line="-87"/>
        <source>Confirm the send action</source>
        <translation>გაგზავნის დადასტურება</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;end</source>
        <translation>გაგ&amp;ზავნა</translation>
    </message>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="-231"/>
        <source>Confirm send coins</source>
        <translation>მონეტების გაგზავნის დადასტურება</translation>
    </message>
    <message>
        <location line="-52"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>%1 to %2</source>
        <translation>%1-დან %2-ში</translation>
    </message>
    <message>
        <location line="-289"/>
        <source>Copy quantity</source>
        <translation>რაოდენობის კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>თანხის კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy fee</source>
        <translation>საკომისიოს კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>დამატებითი საკომისიოს კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>ბაიტების კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>პრიორიტეტის კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>ხურდის კოპირება</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>&lt;b&gt;Public label:&lt;/b&gt; %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Total Amount %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>or</source>
        <translation>ან</translation>
    </message>
    <message>
        <location line="+187"/>
        <source>The recipient address is not valid. Please recheck.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount to pay must be larger than 0.</source>
        <translation>გადახდის მოცულობა 0-ზე მეტი უნდა იყოს</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount exceeds your balance.</source>
        <translation>თანხა აღემატება თქვენს ბალანსს</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The total exceeds your balance when the %1 transaction fee is included.</source>
        <translation>საკომისიო %1-ის დამატების შემდეგ თანხა აჭარბებს თქვენს ბალანსს</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Duplicate address found: addresses should only be used once each.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction creation failed!</source>
        <translation>შეცდომა ტრანსაქციის შექმნისას!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The transaction was rejected! This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here.</source>
        <translation>ტრანსაქცია უარყოფილია! შესაძლოა მონეტების ნაწილი თქვენი საფულიდან უკვე გამოყენებულია, რაც შეიძლება მოხდეს wallet.dat-ის ასლის გამოყენებისას, როცა მონეტები გაიგზავნა სხვა ასლიდან, აქ კი არ არის გაგზავნილად მონიშნული.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A fee higher than %1 is considered an absurdly high fee.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Payment request expired.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Public Label exeeds limit of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Pay only the required fee of %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Warning: Invalid Nexa address</source>
        <translation>ყურადღება: არასწორია Nexa-მისამართი</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Confirm custom change address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The address you selected for change is not part of this wallet. Any or all funds in your wallet may be sent to this address. Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>(no label)</source>
        <translation>(არ არის ნიშნული)</translation>
    </message>
    <message>
        <location line="-28"/>
        <source>Warning: Unknown change address</source>
        <translation>ყურადღება: უცნობია ხურდის მისამართი</translation>
    </message>
    <message>
        <location line="-486"/>
        <source>Are you sure you want to send?</source>
        <translation>დარწმუნებული ხართ, რომ გინდათ გაგზავნა?</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>added as transaction fee</source>
        <translation>დამატებულია საკომისიო</translation>
    </message>
</context>
<context>
    <name>SendCoinsEntry</name>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="+86"/>
        <location line="+677"/>
        <location line="+560"/>
        <source>A&amp;mount:</source>
        <translation>&amp;რაოდენობა</translation>
    </message>
    <message>
        <location line="-1224"/>
        <source>Pay &amp;To:</source>
        <translation>ადრესა&amp;ტი:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Choose previously used address</source>
        <translation>აირჩიეთ ადრე გამოყენებული მისამართი</translation>
    </message>
    <message>
        <location line="-87"/>
        <source>Amount to send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The fee will be deducted from the amount being sent. The recipient will receive less coins than you enter in the amount field. If multiple recipients are selected, the fee is split equally.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;ubtract fee from amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Private Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Private &amp;Label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+51"/>
        <source>The Nexa address to send the payment to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>მისამართის ჩასმა კლიპბორდიდან</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+553"/>
        <location line="+560"/>
        <source>Remove this entry</source>
        <translation>ჩანაწერის წაშლა</translation>
    </message>
    <message>
        <location line="-1091"/>
        <source>A message that was attached to the coin: URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter a private label for this address to add it to the list of used addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <location filename="../sendcoinsentry.cpp" line="+43"/>
        <source>Enter a public label for this transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Public Label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+466"/>
        <source>This is an unauthenticated payment request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+556"/>
        <source>This is an authenticated payment request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sendcoinsentry.cpp" line="-12"/>
        <source>A message that was attached to the %1 URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>მესიჯი, რომელიც თან ერთვის %1 URI, რომელიც შეინახება ტრანსაქციასთან ერთად თქვენთვის. შენიშვნა: მესიჯი არ გაყვება გადახდას ბითქოინის ქსელში.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Enter a private label for this address to add it to your address book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="-541"/>
        <location line="+556"/>
        <source>Pay To:</source>
        <translation>ადრესატი:</translation>
    </message>
    <message>
        <location line="-522"/>
        <location line="+560"/>
        <source>Memo:</source>
        <translation>შენიშვნა:</translation>
    </message>
</context>
<context>
    <name>ShutdownWindow</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+76"/>
        <source>%1 is shutting down...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not shut down the computer until this window disappears.</source>
        <translation>არ გამორთოთ კომპიუტერი ამ ფანჯრის გაქრობამდე.</translation>
    </message>
</context>
<context>
    <name>SignVerifyMessageDialog</name>
    <message>
        <location filename="../forms/signverifymessagedialog.ui" line="+14"/>
        <source>Signatures - Sign / Verify a Message</source>
        <translation>ხელმოწერები - მესიჯის ხელმოწერა/ვერიფიკაცია</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sign Message</source>
        <translation>მე&amp;სიჯის ხელმოწერა</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You can sign messages/agreements with your addresses to prove you can receive coins sent to them. Be careful not to sign anything vague or random, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The Nexa address to sign the message with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+210"/>
        <source>Choose previously used address</source>
        <translation>აირჩიეთ ადრე გამოყენებული მისამართი</translation>
    </message>
    <message>
        <location line="-200"/>
        <location line="+210"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="-200"/>
        <source>Paste address from clipboard</source>
        <translation>მისამართის ჩასმა კლიპბორდიდან</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter the message you want to sign here</source>
        <translation>აკრიფეთ ხელმოსაწერი მესიჯი</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Signature</source>
        <translation>ხელმოწერა</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation>მიმდინარე ხელმოწერის კოპირება კლიპბორდში</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Sign the message to prove you own this Nexa address</source>
        <translation>მოაწერეთ ხელი იმის დასადასტურებლად, რომ ეს მისამართი თქვენია</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sign &amp;Message</source>
        <translation>&amp;მესიჯის ხელმოწერა</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all sign message fields</source>
        <translation>ხელმოწერის ყველა ველის წაშლა</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+143"/>
        <source>Clear &amp;All</source>
        <translation>გ&amp;ასუფთავება</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>&amp;Verify Message</source>
        <translation>მესიჯის &amp;ვერიფიკაცია</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the receiver&apos;s address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack. Note that this only proves the signing party receives with the address, it cannot prove sendership of any transaction!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The Nexa address the message was signed with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Verify the message to ensure it was signed with the specified Nexa address</source>
        <translation>შეამოწმეთ, რომ მესიჯი ხელმოწერილია მითითებული Nexa-მისამართით</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify &amp;Message</source>
        <translation>&amp;მესიჯის ვერიფიკაცია</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all verify message fields</source>
        <translation>ვერიფიკაციის ყველა ველის წაშლა</translation>
    </message>
    <message>
        <location filename="../signverifymessagedialog.cpp" line="+40"/>
        <source>Click &quot;Sign Message&quot; to generate signature</source>
        <translation>ხელმოწერის გენერირებისათვის დააჭირეთ &quot;მესიჯის ხელმოწერა&quot;-ს</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Wallet unlock was cancelled.</source>
        <translation>საფულის განბლოკვა შეწყვეტილია.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Message signed.</source>
        <translation>მესიჯი ხელმოწერილია.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Message verified.</source>
        <translation>მესიჯი ვერიფიცირებულია.</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../networkstyle.cpp" line="+19"/>
        <source>[testnet]</source>
        <translation>[testnet]</translation>
    </message>
</context>
<context>
    <name>TokenDescDialog</name>
    <message>
        <location filename="../forms/tokendescdialog.ui" line="+17"/>
        <source>Token details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This pane shows a detailed description of the token&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TokenDisplayDialog</name>
    <message>
        <location filename="../forms/tokendisplaydialog.ui" line="+14"/>
        <source>Token Display Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add a token to the list below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add New Token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove a token from the list below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove Token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ticker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Group ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tokendisplaydialog.cpp" line="+43"/>
        <source>Copy GroupID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Token Ticker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TokenHistoryView</name>
    <message>
        <location filename="../tokenhistoryview.cpp" line="+78"/>
        <location line="+19"/>
        <source>All</source>
        <translation type="unfinished">ყველა</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation type="unfinished">დღეს</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation type="unfinished">ამ კვირის</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation type="unfinished">ამ თვის</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation type="unfinished">ბოლო თვის</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation type="unfinished">ამ წლის</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation type="unfinished">შუალედი...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation type="unfinished">შემოსულია</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation type="unfinished">საკუთარი თავისათვის</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation type="unfinished">სხვა</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter token ID to search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation type="unfinished">მინ. თანხა</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy token id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation type="unfinished">თანხის კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation type="unfinished">ტრანსაქციის დეტალების ჩვენება</translation>
    </message>
    <message>
        <location line="+167"/>
        <source>Export Token History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation type="unfinished">CSV-ფაილი (*.csv)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Confirmed</source>
        <translation type="unfinished">დადასტურებულია</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation type="unfinished">თარიღი</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation type="unfinished">ტიპი</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation type="unfinished">ნიშნული</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation type="unfinished">მისამართი</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction Idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token Amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation type="unfinished">ექსპორტი ვერ განხორციელდა</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the token history to %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation type="unfinished">ეხპორტი განხორციელებულია</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The token history was successfully saved to %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Range:</source>
        <translation type="unfinished">შუალედი:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation type="unfinished">-</translation>
    </message>
</context>
<context>
    <name>TokenTableModel</name>
    <message>
        <location filename="../tokentablemodel.cpp" line="+354"/>
        <source>Date</source>
        <translation type="unfinished">თარიღი</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation type="unfinished">ტიპი</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Token ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Net Amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location line="+60"/>
        <source>Open for %n more block(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open until %1</source>
        <translation type="unfinished">ღია იქნება სანამ %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation type="unfinished">ოფლაინშია</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation type="unfinished">დაუდასტურებელია</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation type="unfinished">დადასტურებულია (%1,  რეკომენდებულია %2)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation type="unfinished">დადასტურებულია (%1დასტური)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation type="unfinished">კონფლიქტშია</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation type="unfinished">არ არის მომწიფებული (%1 დასტური, საჭიროა სულ %2)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation type="unfinished">ეს ბლოკი არ არის მიღებული არცერთი კვანძის მიერ და სავარაუდოდ უარყოფილია!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation type="unfinished">გენერირებულია, მაგრამ უარყოფილია</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation type="unfinished">შემოსულია</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation type="unfinished">გამომგზავნი</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation type="unfinished">გადახდილია საკუთარი თავისათვის</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Melt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation type="unfinished">სხვა</translation>
    </message>
    <message>
        <location line="+325"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation type="unfinished">ტრანსაქციის სტატუსი. ველზე კურსორის შეყვანისას გამოჩნდება დასტურების რაოდენობა.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation type="unfinished">ტრანსაქციის მიღების თარიღი და დრო.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation type="unfinished">ტრანსაქციის ტიპი.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation type="unfinished">ბალანსიდან მოხსნილი ან დამატებული თანხა.</translation>
    </message>
</context>
<context>
    <name>TokensViewDialog</name>
    <message>
        <location filename="../forms/tokensviewdialog.ui" line="+14"/>
        <source>Tokens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable/Disable which tokens in the wallet are to be displayed in QT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Manage Displayed Tokens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tokens in Wallet:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tokens Displayed: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Amount: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The number of tokens to send&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>  Pay To: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The Nexa address to send the payment to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation type="unfinished">აირჩიეთ ადრე გამოყენებული მისამართი</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+A</source>
        <translation type="unfinished">Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation type="unfinished">მისამართის ჩასმა კლიპბორდიდან</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+P</source>
        <translation type="unfinished">Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remove this entry</source>
        <translation type="unfinished">ჩანაწერის წაშლა</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Send tokens&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Token ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The unique token identifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-45"/>
        <source>The token group identifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Name</source>
        <translation type="unfinished">სახელი</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The name of the token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ticker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The token ticker symbol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed token balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed token balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Sub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>If checked then this item is a subgroup.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tokensviewdialog.cpp" line="+397"/>
        <location line="+7"/>
        <location line="+4"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-11"/>
        <source>string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+4"/>
        <source>num</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>NaN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+84"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Total Mintage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-93"/>
        <location line="+94"/>
        <source>Token mintage is unavailable because the database needs a reindex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-86"/>
        <location line="+43"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Ticker:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+43"/>
        <source>URL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Hash:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-39"/>
        <location line="+43"/>
        <source>Decimals:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-37"/>
        <location line="+14"/>
        <location line="+62"/>
        <location line="+14"/>
        <source>Current Authorities:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-88"/>
        <location line="+76"/>
        <source>Mint:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Melt:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Renew:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Rescript:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Subgroup:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+76"/>
        <source>Token authorities are unavailable because the database needs a reindex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+77"/>
        <source>Balance:</source>
        <translation type="unfinished">ბალანსი:</translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+77"/>
        <source>Pending:</source>
        <translation type="unfinished">იგზავნება:</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Are you sure you want to send?</source>
        <translation type="unfinished">დარწმუნებული ხართ, რომ გინდათ გაგზავნა?</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;b&gt;Token(s)&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Confirm send tokens</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrafficGraphWidget</name>
    <message>
        <location filename="../trafficgraphwidget.cpp" line="+74"/>
        <source>KB/s</source>
        <translation>KB/s</translation>
    </message>
</context>
<context>
    <name>TransactionDesc</name>
    <message>
        <location filename="../transactiondesc.cpp" line="+35"/>
        <source>Open until %1</source>
        <translation>ღია იქნება სანამ %1</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>conflicted</source>
        <translation>კონფლიქტშია</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/offline</source>
        <translation>%1/გათიშულია</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/unconfirmed</source>
        <translation>%1/დაუდასტურებელია</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations</source>
        <translation>%1 დადასტურებულია</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Status</source>
        <translation>სტატუსი</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Date</source>
        <translation>თარიღი</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Source</source>
        <translation>წყარო</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Generated</source>
        <translation>გენერირებულია</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+13"/>
        <location line="+122"/>
        <source>From</source>
        <translation>გამგზავნი</translation>
    </message>
    <message>
        <location line="-142"/>
        <location line="+21"/>
        <location line="+35"/>
        <location line="+108"/>
        <source>To</source>
        <translation>მიმღები</translation>
    </message>
    <message numerus="yes">
        <location line="-217"/>
        <source>Open for %n more block(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+8"/>
        <source>double spent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>abandoned</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location line="+32"/>
        <source>, broadcast through %n node(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+38"/>
        <source>change address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>own address</source>
        <translation>საკუთარი მისამართი</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+113"/>
        <source>watch-only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-110"/>
        <location line="+28"/>
        <location line="+107"/>
        <source>label</source>
        <translation>ნიშნული</translation>
    </message>
    <message>
        <location line="-116"/>
        <location line="+57"/>
        <location line="+48"/>
        <location line="+71"/>
        <source>Public label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-149"/>
        <location line="+41"/>
        <location line="+63"/>
        <location line="+47"/>
        <location line="+256"/>
        <source>Credit</source>
        <translation>კრედიტი</translation>
    </message>
    <message numerus="yes">
        <location line="-404"/>
        <source>matures in %n more block(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not accepted</source>
        <translation>უარყოფილია</translation>
    </message>
    <message>
        <location line="+102"/>
        <location line="+31"/>
        <location line="+261"/>
        <source>Debit</source>
        <translation>დებიტი</translation>
    </message>
    <message>
        <location line="-280"/>
        <source>Total debit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Total credit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction fee</source>
        <translation>ტრანსაქციის საფასური - საკომისიო</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Net amount</source>
        <translation>სუფთა თანხა</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+13"/>
        <source>Message</source>
        <translation>მესიჯი</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Comment</source>
        <translation>შენიშვნა</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction Idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Merchant</source>
        <translation>გამყიდველი</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Generated coins must mature %1 blocks before they can be spent. When you generated this block, it was broadcast to the network to be added to the block chain. If it fails to get into the chain, its state will change to &quot;not accepted&quot; and it won&apos;t be spendable. This may occasionally happen if another node generates a block within a few seconds of yours.</source>
        <translation>გენერირებული მონეტები გასაგზავნად მომწიფდება %1 ბლოკის შემდეგ. ეს ბლოკი გენერირების შემდეგ გავრცელებულ იქნა ქსელში ბლოკთა ჯაჭვზე დასამატებლად. თუ ის ვერ ჩაჯდა ჯაჭვში, მიეცემა სტატუსი &quot;უარყოფილია&quot; და ამ მონეტებს ვერ გამოიყენებთ. ასეთი რამ შეიძლება მოხდეს, თუ რომელიმე კვანძმა რამდენიმე წამით დაგასწროთ ბლოკის გენერირება.</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Token ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Ticker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-80"/>
        <location line="+33"/>
        <location line="+48"/>
        <source>Name</source>
        <translation type="unfinished">სახელი</translation>
    </message>
    <message>
        <location line="-77"/>
        <location line="+32"/>
        <location line="+48"/>
        <source>Decimals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+4"/>
        <source>Melt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+4"/>
        <location line="+56"/>
        <location line="+4"/>
        <source>Mint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+4"/>
        <source>Amount Sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+4"/>
        <source>Amount Received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Debug information</source>
        <translation>დახვეწის ინფორმაცია</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transaction</source>
        <translation>ტრანსაქცია</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inputs</source>
        <translation>ხარჯები</translation>
    </message>
    <message>
        <location line="-133"/>
        <location line="+4"/>
        <location line="+30"/>
        <location line="+4"/>
        <location line="+56"/>
        <location line="+4"/>
        <location line="+57"/>
        <source>Amount</source>
        <translation>თანხა</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+3"/>
        <source>true</source>
        <translation>ჭეშმარიტი</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+3"/>
        <source>false</source>
        <translation>მცდარი</translation>
    </message>
    <message>
        <location line="-533"/>
        <source>, has not been successfully broadcast yet</source>
        <translation>, დაგზავნა არ არის წარმატებით დასრულებული</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>unknown</source>
        <translation>უცნობია</translation>
    </message>
</context>
<context>
    <name>TransactionDescDialog</name>
    <message>
        <location filename="../forms/transactiondescdialog.ui" line="+14"/>
        <source>Transaction details</source>
        <translation>ტრანსაქციის დეტალები</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>This pane shows a detailed description of the transaction</source>
        <translation>ტრანსაქციის დაწვრილებითი აღწერილობა</translation>
    </message>
</context>
<context>
    <name>TransactionGraphWidget</name>
    <message>
        <location filename="../transactiongraphwidget.cpp" line="+148"/>
        <source>tps</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransactionTableModel</name>
    <message>
        <location filename="../transactiontablemodel.cpp" line="+239"/>
        <source>Date</source>
        <translation>თარიღი</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>ტიპი</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>არ არის მომწიფებული (%1 დასტური, საჭიროა სულ %2)</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Open until %1</source>
        <translation>ღია იქნება სანამ %1</translation>
    </message>
    <message>
        <location line="-63"/>
        <source>Address or Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location line="+60"/>
        <source>Open for %n more block(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>დადასტურებულია (%1დასტური)</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>ეს ბლოკი არ არის მიღებული არცერთი კვანძის მიერ და სავარაუდოდ უარყოფილია!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>გენერირებულია, მაგრამ უარყოფილია</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Offline</source>
        <translation>ოფლაინშია</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>დაუდასტურებელია</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>დადასტურებულია (%1,  რეკომენდებულია %2)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Conflicted</source>
        <translation>კონფლიქტშია</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation>შემოსულია</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>გამომგზავნი</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent to</source>
        <translation>გაგზავნილია</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>გადახდილია საკუთარი თავისათვის</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mined</source>
        <translation>მოპოვებულია</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation type="unfinished">სხვა</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>watch-only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+277"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>ტრანსაქციის სტატუსი. ველზე კურსორის შეყვანისას გამოჩნდება დასტურების რაოდენობა.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>ტრანსაქციის მიღების თარიღი და დრო.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>ტრანსაქციის ტიპი.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>ბალანსიდან მოხსნილი ან დამატებული თანხა.</translation>
    </message>
</context>
<context>
    <name>TransactionView</name>
    <message>
        <location filename="../transactionview.cpp" line="+77"/>
        <location line="+19"/>
        <source>All</source>
        <translation>ყველა</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>დღეს</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>ამ კვირის</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>ამ თვის</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>ბოლო თვის</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>ამ წლის</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>შუალედი...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>შემოსულია</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent to</source>
        <translation>გაგზავნილია</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>საკუთარი თავისათვის</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mined</source>
        <translation>მოპოვებულია</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>სხვა</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter address or label to search</source>
        <translation>შეიყვანეთ საძებნი მისამართი ან ნიშნული</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>მინ. თანხა</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy address</source>
        <translation>მისამართის კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>ნიშნულის კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>თანხის კოპირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit label</source>
        <translation>ნიშნულის რედაქტირება</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>ტრანსაქციის დეტალების ჩვენება</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Export Transaction History</source>
        <translation>ტრანსაქციების ისტორიის ექსპორტი</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation>ექსპორტი ვერ განხორციელდა</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the transaction history to %1.</source>
        <translation>შეცდომა %1-ში ტრანსაქციების შენახვის მცდელობისას.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>ეხპორტი განხორციელებულია</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction history was successfully saved to %1.</source>
        <translation>ტრანსაქციების ისტორია შენახულია %1-ში.</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Comma separated file (*.csv)</source>
        <translation>CSV-ფაილი (*.csv)</translation>
    </message>
    <message>
        <location line="-179"/>
        <source>Copy transaction idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+187"/>
        <source>Confirmed</source>
        <translation>დადასტურებულია</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>თარიღი</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>ტიპი</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>ნიშნული</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>მისამართი</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Range:</source>
        <translation>შუალედი:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>-</translation>
    </message>
</context>
<context>
    <name>UnitDisplayStatusBarControl</name>
    <message>
        <location filename="../nexagui.cpp" line="+117"/>
        <source>Unit to show amounts in. Click to select another unit.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnlimitedDialog</name>
    <message>
        <location filename="../forms/unlimited.ui" line="+14"/>
        <source>Unlimited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Mining</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+10"/>
        <source>The largest block that will be mined</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Maximum Generated Block Size (bytes) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Network</source>
        <translation type="unfinished">&amp;ქსელი</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Bandwidth Restrictions in KBytes/sec (check to enable):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+71"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-35"/>
        <location line="+59"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Receive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Active command-line options that override above options:</source>
        <translation type="unfinished">საკომანდო სტრიქონის აქტიური ოპციები, რომლებიც გადაფარავენ ზემოთნაჩვენებს:</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Reset all client options to default.</source>
        <translation type="unfinished">კლიენტის ყველა პარამეტრის დაბრუნება ნაგულისხმევ მნიშვნელობებზე.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation type="unfinished">დაბ&amp;რუნების ოპციები</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;გაუქმება</translation>
    </message>
    <message>
        <location filename="../unlimiteddialog.cpp" line="-345"/>
        <source>Confirm options reset</source>
        <translation type="unfinished">დაადასტურეთ პარამეტრების დაბრუნება ნაგულისხმევზე</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is a global reset of all settings!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client restart required to activate changes.</source>
        <translation type="unfinished">ცვლილებები ძალაში შევა კლიენტის ხელახალი გაშვების შემდეგ.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+39"/>
        <location line="+42"/>
        <location line="+42"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-108"/>
        <location line="+36"/>
        <location line="+43"/>
        <location line="+41"/>
        <source>Traffic shaping parameters have to be greater than 0.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WalletFrame</name>
    <message>
        <location filename="../walletframe.cpp" line="+31"/>
        <source>No wallet has been loaded.</source>
        <translation>არ არის ჩატვირთული საფულე.</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Token Database Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token wallet not available because a -reindex is needed. Click &quot;Ok&quot; to perform a one time -reindex on the next startup. Then shutdown Nexa-Qt and restart to complete the process.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WalletModel</name>
    <message>
        <location filename="../walletmodel.cpp" line="+300"/>
        <source>Send Coins</source>
        <translation>მონეტების გაგზავნა</translation>
    </message>
</context>
<context>
    <name>WalletView</name>
    <message>
        <location filename="../walletview.cpp" line="+46"/>
        <location line="+20"/>
        <source>&amp;Export</source>
        <translation>&amp;ექსპორტი</translation>
    </message>
    <message>
        <location line="-19"/>
        <location line="+20"/>
        <source>Export the data in the current tab to a file</source>
        <translation>ამ ბარათიდან მონაცემების ექსპორტი ფაილში</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Backup Wallet</source>
        <translation>საფულის არქივირება</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+21"/>
        <source>Wallet Data (*.dat)</source>
        <translation>საფულის მონაცემები (*.dat)</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Backup Failed</source>
        <translation>არქივირება ვერ მოხერხდა</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the wallet data to %1.</source>
        <translation>შეცდომა %1-ში საფულის მონაცემების შენახვის მცდელობისას.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The wallet data was successfully saved to %1.</source>
        <translation>საფულის მონაცემები შენახულია %1-ში.</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Backup Successful</source>
        <translation>არქივირება შესრულებულია</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Restore Wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Restore Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to restore the wallet data to %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>nexa</name>
    <message>
        <location filename="../nexastrings.cpp" line="+76"/>
        <source>This is a pre-release test build - use at your own risk - do not use for mining or merchant applications</source>
        <translation>ეს არის წინასწარი სატესტო ვერსია - გამოიყენეთ საკუთარი რისკით - არ გამოიყენოთ მოპოვებისა ან კომერციული მიზნებისათვის</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.</source>
        <translation>ყურადღება: ქსელში შეუთანხმებლობაა. შესაძლოა ცალკეულ მომპოვებლებს პრობლემები ექმნებათ!</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.</source>
        <translation>ყურადღება: ჩვენ არ ვეთანხმებით ყველა პირს. შესაძლოა თქვენ ან სხვა კვანძებს განახლება გჭირდებათ.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>You can not run &quot;-salvagewallet&quot; as an HD wallet.

Please relaunch Nexa with &quot;-usehd=0&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You need to rebuild the database using -resync to go back to unpruned mode.  This will redownload the entire blockchain.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Corrupted block database detected</source>
        <translation>შენიშნულია ბლოკთა ბაზის დაზიანება</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to rebuild the block database now?</source>
        <translation>გავუშვათ ბლოკთა ბაზის ხელახლა აგება ეხლა?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error initializing block database</source>
        <translation>ვერ ინიციალიზდება ბლოკების ბაზა</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing wallet database environment %s!</source>
        <translation>ვერ ინიციალიზდება საფულის ბაზის გარემო %s!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error opening block database</source>
        <translation>ბლოკთა ბაზის შექმნა ვერ მოხერხდა</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: Disk space is low!</source>
        <translation>შეცდომა: დისზე არ არის ადგილი!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Failed to listen on any port. Use -listen=0 if you want this.</source>
        <translation>ვერ ხერხდება პორტების მიყურადება. თუ გსურთ, გამოიყენეთ -listen=0.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Incorrect or no genesis block found. Wrong datadir for network?</source>
        <translation>საწყისი ბლოკი არ არსებობს ან არასწორია. ქსელის მონაცემთა კატალოგი datadir ხომ არის არასწორი?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid -onion address: &apos;%s&apos;</source>
        <translation>არასწორია მისამართი -onion: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Not enough file descriptors available.</source>
        <translation>არ არის საკმარისი ფაილ-დესკრიპტორები.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Signing token transaction failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Verifying blocks...</source>
        <translation>ბლოკების ვერიფიკაცია...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying wallet...</source>
        <translation>საფულის ვერიფიკაცია...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Waiting for Genesis Block...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet %s resides outside data directory %s</source>
        <translation>საფულე %s მდებარეობს მონაცემთა კატალოგის %s გარეთ</translation>
    </message>
    <message>
        <location line="-72"/>
        <source>Cannot resolve -whitebind address: &apos;%s&apos;</source>
        <translation>ვერ ხერხდება -whitebind მისამართის გარკვევა: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Information</source>
        <translation>ინფორმაცია</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Signing transaction failed</source>
        <translation>ტრანსაქციების ხელმოწერა ვერ მოხერხდა</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Starting txindex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction amount is too small to pay the fee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is experimental software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amount too small</source>
        <translation>ტრანსაქციების რაოდენობა ძალიან ცოტაა</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amounts must be positive</source>
        <translation>ტრანსაქციების რაოდენობა დადებითი რიცხვი უნდა იყოს</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Warning</source>
        <translation>გაფრთხილება</translation>
    </message>
    <message>
        <location line="-42"/>
        <source>Loading addresses...</source>
        <translation>მისამართების ჩატვირთვა...</translation>
    </message>
    <message>
        <location line="-112"/>
        <source>Rescans are not possible in pruned mode. You will need to use -resync which will download the whole blockchain again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Invalid -proxy address: &apos;%s&apos;</source>
        <translation>არასწორია მისამართი -proxy: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Transaction has %d outputs. Maximum outputs allowed is %d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction of %d bytes is too large. Maximum allowed is %d bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large for fee policy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Turn off auto consolidate and try sending again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to bind to %s on this computer (bind returned error %s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to bind to %s on this computer. %s is probably already running.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to start RPC services. See debug log for details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown network specified in -onlynet: &apos;%s&apos;</source>
        <translation>-onlynet-ში მითითებულია უცნობი ქსელი: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Cannot resolve -bind address: &apos;%s&apos;</source>
        <translation>ვერ ხერხდება -bind მისამართის გარკვევა: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -externalip address: &apos;%s&apos;</source>
        <translation>ვერ ხერხდება -externalip მისამართის გარკვევა: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Loading block index...</source>
        <translation>ბლოკების ინდექსის ჩატვირთვა...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet...</source>
        <translation>საფულის ჩატვირთვა...</translation>
    </message>
    <message>
        <location line="-38"/>
        <source>Cannot downgrade wallet</source>
        <translation>საფულის ძველ ვერსიაზე გადაყვანა შეუძლებელია</translation>
    </message>
    <message>
        <location line="-131"/>
        <source>Nexa</source>
        <translation type="unfinished">Nexa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The %s developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bitcoin Bitcoin XT and Bitcoin Unlimited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-wallet.maxTxFee is set very high! Fees this large could be paid on a single transaction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>-wallet.payTxFee is set very high! This is the transaction fee you will pay if you send a transaction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot obtain a lock on data directory %s. %s is probably already running.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Could not locate RPC credentials. No authentication cookie could be found, and no rpcpassword is set in the configuration file (%s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; contained invalid data - see debug.log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Distributed under the MIT software license, see the accompanying file COPYING or &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error loading %s: You can&apos;t enable HD on a already existing non-HD wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading %s! All keys read correctly, but transaction data or address book entries might be missing or incorrect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error reading from the coin database.
Details: %s

Do you want to reindex on the next restart?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: Listening for incoming connections failed (listen returned error %s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Failed to listen on all P2P ports. Failing as requested by -bindallorfail.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fee: %ld is larger than configured maximum allowed fee of : %ld.  To change, set &apos;wallet.maxTxFee&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.maxTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least the minrelay fee of %s to prevent stuck transactions)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.payTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least %s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please check that your computer&apos;s date and time are correct! If your clock is wrong, %s will not work properly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Prune configured below the minimum of %d MiB.  Please use a higher number.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Prune: last wallet synchronisation goes beyond pruned data. You need to -reindex (download the whole blockchain again in case of pruned node)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reducing -maxconnections from %d to %d because of file descriptor limitations (unix) or winsocket fd_set limitations (windows). If you are a windows user there is a hard upper limit of 1024 which cannot be changed by adjusting the node&apos;s configuration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The block database contains a block which appears to be from the future. This may be due to your computer&apos;s date and time being set incorrectly. Only rebuild the block database if you are sure that your computer&apos;s date and time are correct</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The transaction amount is too small to send after the fee has been deducted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit &lt;https://www.openssl.org/&gt; and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Total length of network version string with uacomments added exceeded the maximum length (%i) and have been truncated.  Reduce the number or size of uacomments to avoid truncation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transaction has %d inputs and %d outputs. Maximum inputs allowed are %d and maximum outputs are %d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction has %d inputs. Maximum inputs allowed is %d. Try reducing inputs by transferring a smaller amount.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: abnormally high number of blocks generated, %d blocks received in the last %d hours (%d expected)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: check your network connection, %d blocks received in the last %d hours (%d expected)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Wallet is not password protected. Your funds may be at risk! Goto &quot;Settings&quot; and then select &quot;Encrypt Wallet&quot; to create a password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Could not open deployment configuration CSV file &apos;%s&apos; for reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: Unknown block versions being mined! It&apos;s possible unknown rules are in effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Wallet file corrupt, data salvaged! Original %s saved as %s in %s; if your balance or transactions are incorrect you should restore from a backup.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You are trying to restore the same wallet which you are trying to replace.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>You are trying to use -wallet.auto but neither -spendzeroconfchange nor -wallet.instant is turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You can not send free transactions if you have configured a -relay.limitFreeRelay of zero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&quot;Restore Wallet&quot; succeeded and a backup of the previous wallet was saved to: %s.


When you click &quot;OK&quot; Nexa will shutdown to complete the process.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>%s </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%s corrupt, salvage failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-maxtxpool must be at least %d MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-xthinbloomfiltersize must be at least %d Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Activating best chain...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cannot write default address</source>
        <translation>ვერ ხერხდება ნაგულისხმევი მისამართის ჩაწერა</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CommitTransaction failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright (C) 2015-%i The Bitcoin Unlimited Developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Deployment configuration file &apos;%s&apos; not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet corrupted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet requires newer version of %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: You can&apos;t disable HD on a already existing HD wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: A fatal internal error occurred, see debug.log for details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error: Keypool ran out, please call keypoolrefill first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Importing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Initialization sanity check failed. %s is shutting down.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds for this token.  Need %d more.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds or funds not confirmed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid netmask specified in -whitelist: &apos;%s&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Keypool ran out, please call keypoolrefill first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading Orphanpool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading TxPool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading banlist...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Need to specify a port with -whitebind: &apos;%s&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Opening Block database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Coins Cache database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Description database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Mintage database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening UTXO database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2014-%i The Bitcoin XT Developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune cannot be configured with a negative value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune mode is incompatible with -txindex.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pruning blockstore...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reaccepting Wallet Transactions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rescanning...</source>
        <translation>სკანირება...</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Upgrading block database...This could take a while.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User Agent comment (%s) contains unsafe characters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Wallet needed to be rewritten: restart %s to complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zapping all transactions from wallet...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-68"/>
        <source>Done loading</source>
        <translation>ჩატვირთვა დასრულებულია</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error</source>
        <translation>შეცდომა</translation>
    </message>
</context>
</TS>
