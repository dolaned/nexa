```
saveorphanpool

Dumps the orphanpool to disk.

Examples:
> nexa-cli saveorphanpool 
> curl --user myusername --data-binary '{"jsonrpc": "1.0", "id":"curltest", "method": "saveorphanpool", "params": [] }' -H 'content-type: text/plain;' http://127.0.0.1:7227/

```
