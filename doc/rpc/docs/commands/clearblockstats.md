```
clearblockstats

Clears statistics related to compression blocks such as xthin or graphene.

Arguments: None

Example:
> nexa-cli clearblockstats 

```
